# Git notes


* Visualizar Ramas:
    * Listar ramas en remoto: git branch -r
    * Listar ramas en local: git branch -a

* Guardar cambios en temporal: git stash

* Revertir guardado de cambios en temporal: git stash pop

* Guardar en el indice ficheros especificos para que no les afecte git stash: git add <ruta completa y nombre del fichero> y git stash --keep-index

* Crear rama en local y cambiar a ésta: git checkout -b <nombre de la rama>

* Cambiar upstream de la rama local <la rama en la que se trabaja> para subir cambios a remoto: git branch --set-upstream-to origin <nuevo nombre en remoto>

* Pasar cambios al Head del repo Local: git commit -m "mensaje"

* Pasar cambios del Head del repo Local a la rama del repo Remoto: git push origin <nombre de la rama enviar cambios>

* Eliminar rama: git branch -d the_local_branch

* Actualizar la rama desde master: https://gist.github.com/santisbon/a1a60db1fb8eecd1beeacd986ae5d3ca

* Subir nuevo proyecto a repo existente: https://stackoverflow.com/questions/17291995/push-existing-project-into-github
